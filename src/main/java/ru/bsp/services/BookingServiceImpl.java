package ru.bsp.services;

import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bsp.forms.BookingForm;
import ru.bsp.models.Book;
import ru.bsp.models.Booking;
import ru.bsp.repositories.BookingRepository;
import ru.bsp.repositories.BooksRepository;

import java.util.ArrayList;
import java.util.List;

//@RequiredArgsConstructor
@Component
public class BookingServiceImpl implements BookingsService {
    @Autowired
    private BookingRepository bookingRepository;
    @Autowired
    private BooksRepository booksRepository;

    @Override
    public void addBooking(BookingForm bookingForm){
        Booking booking = Booking.builder()
                .booking_date(bookingForm.getBooking_date())
                .booking_count(bookingForm.getBooking_count())
                .owner(bookingForm.getOwner())
                .ownerClient(bookingForm.getOwnerClient())
                .build();
        bookingRepository.save(booking);

        Book book = booksRepository.getById(bookingForm.getOwner().getId());
        book.setBooks_count(book.getBooks_count() - bookingForm.getBooking_count());
        booksRepository.save(book);
    }

    @Override
    public List<Booking> getAllBookings() {
        return bookingRepository.findAll();
    }
}
