package ru.bsp.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.bsp.exceptions.BookNotFoundException;
import ru.bsp.forms.ClientForm;
import ru.bsp.models.Book;
import ru.bsp.models.Client;
import ru.bsp.repositories.ClientRepository;
import java.util.List;

@RequiredArgsConstructor
@Component
public class ClientsServiceImpl implements ClientsService{


    private final ClientRepository clientRepository;

    @Override
    public void addClient(ClientForm form) {
        Client client = Client.builder()
                .name(form.getName())
                .build();
        clientRepository.save(client);

    }

    @Override
    public List<Client> getAllClients() {
        return clientRepository.findAll();
    }

    @Override
    public void deleteClient(Integer clientId) {
        clientRepository.deleteById(clientId);
    }

    @Override
    public Client getClient(Integer clientId) {
        return clientRepository.findById(clientId).orElseThrow(BookNotFoundException::new);
    }
}
