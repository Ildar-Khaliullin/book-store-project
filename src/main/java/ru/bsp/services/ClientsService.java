package ru.bsp.services;

import ru.bsp.forms.ClientForm;
import ru.bsp.models.Client;
import java.util.List;

public interface ClientsService {
    void addClient(ClientForm form);

    List<Client> getAllClients();

    void deleteClient(Integer clientId);

    Client getClient(Integer clientId);
}
