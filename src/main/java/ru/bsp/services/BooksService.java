package ru.bsp.services;

import ru.bsp.forms.BookForm;
import ru.bsp.models.Book;
import ru.bsp.models.Booking;

import java.util.List;

public interface BooksService {
    void addBook(BookForm form);
    List<Book> getAllBooks();

    void deleteBook(Integer bookId);

    Book getBook(Integer bookId);

    List<Booking> getBookingsByBook(Integer bookId);

    List<Booking> getBookingsByClient(Integer clientId);

    void updateBook(Integer bookId, BookForm bookForm);

    List<Book> getAllBook();
}
