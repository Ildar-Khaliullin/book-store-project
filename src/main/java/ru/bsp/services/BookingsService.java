package ru.bsp.services;

import ru.bsp.forms.BookingForm;
import ru.bsp.models.Booking;

import java.util.List;

public interface BookingsService {

    void addBooking(BookingForm bookingForm);

    List<Booking> getAllBookings();
}
