package ru.bsp.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.bsp.exceptions.BookNotFoundException;
import ru.bsp.forms.BookForm;
import ru.bsp.models.Book;
import ru.bsp.models.Booking;
import ru.bsp.repositories.BookingRepository;
import ru.bsp.repositories.BooksRepository;
import java.util.List;

@RequiredArgsConstructor
@Component
public class BooksServiceImpl implements BooksService {
    private final BooksRepository booksRepository;
    private final BookingRepository bookingRepository;


    @Override
    public void addBook(BookForm form) {
        Book book = Book.builder()
                .book_author(form.getBook_author())
                .book_name(form.getBook_name())
                .genre(form.getGenre())
                .books_count(1)
                .cost(1)
                .description("")
                .build();
        booksRepository.save(book);

    }

    @Override
    public List<Book> getAllBooks() {
        return booksRepository.findAll();
    }

    @Override
    public void deleteBook(Integer bookId) {
        booksRepository.deleteById(bookId);

    }

    @Override
    public Book getBook(Integer bookId) {
        return booksRepository.findById(bookId).orElseThrow(BookNotFoundException::new);
    }

    @Override
    public List<Booking> getBookingsByBook(Integer bookId) {
        return bookingRepository.findAllByOwner_Id(bookId);
    }

    @Override
    public List<Booking> getBookingsByClient(Integer clientId) {
        return bookingRepository.findAllByOwnerClient_Id(clientId);
    }

    @Override
    public void updateBook(Integer bookId, BookForm bookForm) {
        Book book = booksRepository.getById(bookId);
        book.setCost(bookForm.getCost());
        book.setBooks_count(bookForm.getBooks_count());
        book.setDescription(bookForm.getDescription());
        booksRepository.save(book);


    }

    @Override
    public List<Book> getAllBook() {
        return booksRepository.findAll();
    }


}
