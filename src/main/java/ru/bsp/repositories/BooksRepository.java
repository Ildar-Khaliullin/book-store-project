package ru.bsp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.bsp.models.Book;

public interface BooksRepository extends JpaRepository<Book, Integer> {
}
