package ru.bsp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.bsp.models.Client;

public interface ClientRepository extends JpaRepository<Client, Integer> {
}
