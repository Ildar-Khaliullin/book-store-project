package ru.bsp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.bsp.models.Booking;

import java.util.List;

public interface BookingRepository extends JpaRepository<Booking, Integer> {
    List<Booking> findAllByOwner_Id(Integer id);

    List<Booking> findAllByOwnerClient_Id(Integer clientId);
}
