package ru.bsp.repositories.jdbc;
//
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.jdbc.core.RowMapper;
//import org.springframework.stereotype.Component;
//import ru.bsp.forms.BookForm;
//import ru.bsp.models.Book;
//
//import javax.sql.DataSource;
//import java.util.List;
//
//@Component
//public class BooksRepositoryJdbcTemplateImpl implements BooksRepository {
//    //language=SQL
//    private static final String SQL_INSERT = "insert into product(book_author, book_name, genre, cost, books_count) values (?,?,?,?,?)";
//    //language=SQL
//    private static final String SQL_SELECT_ALL = "select * from product order by id";
//    //language=SQL
//    private static final String SQL_DELETE_BY_ID = "delete FROM product where id = ?";
//    //language=SQL
//    private static final String SQL_SELECT_BY_ID = "select * from product where id = ?";
//    //language=SQL
//    private static final String SQL_UPDATE_COST = "UPDATE product set cost = ? where id = ?";
//    //language=SQL
//    private static final String SQL_UPDATE_BOOKS_COUNT = "UPDATE product set books_count = ? where id = ?";
//    //language=SQL
//    private static final String SQL_UPDATE_DESCRIPTION = "UPDATE product set description = ? where id = ?";
//
//    private final JdbcTemplate jdbcTemplate;
//
//    public BooksRepositoryJdbcTemplateImpl(DataSource dataSource) {
//        this.jdbcTemplate = new JdbcTemplate(dataSource);
//    }
//
//    private static final RowMapper<Book> bookRowMapper = (row, rowNum) -> {
//        int id = row.getInt("id");
//        String book_author = row.getString("book_author");
//        String book_name = row.getString("book_name");
//        String genre = row.getString("genre");
//        int cost = row.getInt("cost");
//        int books_count = row.getInt("books_count");
//        String description = row.getString("description");
//        if (description == null) description = "";
//        return new Book(id, book_author, book_name, genre, cost, books_count, description);
//    };
//
//
//    @Override
//    public List<Book> findAll() {
//        return jdbcTemplate.query(SQL_SELECT_ALL, bookRowMapper);
//    }
//
//    @Override
//    public void save(Book book) {
//        jdbcTemplate.update(SQL_INSERT, book.getBook_author(), book.getBook_name(), book.getGenre(),
//                book.getCost(), book.getBooks_count());
//    }
//
//    @Override
//    public void delete(Long bookId) {
//        jdbcTemplate.update(SQL_DELETE_BY_ID, bookId);
//    }
//
//    @Override
//    public Book findById(Long bookId) {
//        return jdbcTemplate.queryForObject(SQL_SELECT_BY_ID, bookRowMapper, bookId);
//    }
//
//
//    @Override
//    public Book update(Long bookId, BookForm bookForm) {
//        jdbcTemplate.update(SQL_UPDATE_COST, bookForm.getCost(), bookId);
//        jdbcTemplate.update(SQL_UPDATE_BOOKS_COUNT, bookForm.getBooks_count(), bookId);
//        jdbcTemplate.update(SQL_UPDATE_DESCRIPTION, bookForm.getDescription(), bookId);
//        return jdbcTemplate.queryForObject(SQL_SELECT_BY_ID, bookRowMapper, bookId);
//    }
//
//}
