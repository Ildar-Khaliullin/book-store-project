package ru.bsp.forms;

import lombok.Data;

@Data
public class BookForm {

    private String book_author;

    private String book_name;

    private String genre;

    private Integer cost;

    private Integer books_count;

    private String description;
}
