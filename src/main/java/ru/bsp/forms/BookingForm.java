package ru.bsp.forms;

import lombok.Data;
import ru.bsp.models.Book;
import ru.bsp.models.Client;

@Data
public class BookingForm {

    private String booking_date;

    private Integer booking_count;

    private Book owner;

    private Client ownerClient;
}
