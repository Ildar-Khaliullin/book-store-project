package ru.bsp.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity

public class Booking {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String booking_date;
    private Integer booking_count;

    @ManyToOne
    @JoinColumn(name = "id_product")
    private Book owner;

    @ManyToOne
    @JoinColumn(name = "id_client")
    private Client ownerClient;


}
