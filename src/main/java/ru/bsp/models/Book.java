package ru.bsp.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "product")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String book_author;
    private String book_name;
    private String genre;

    @Column(columnDefinition = "integer not null default 0")
    private Integer cost;

    @Column(columnDefinition = "integer not null default 0")
    private Integer books_count;

    @Column(columnDefinition = "String not null default   ")
    private String description;

    //??
    @OneToMany(mappedBy = "owner")
    private List<Booking> bookings;


}
