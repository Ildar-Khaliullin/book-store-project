package ru.bsp.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.bsp.forms.BookForm;
import ru.bsp.models.Book;
import ru.bsp.models.Booking;
import ru.bsp.models.Client;
import ru.bsp.services.BooksService;
import ru.bsp.services.ClientsService;
import java.util.List;

@Controller
public class BooksController {

    private final BooksService booksService;
    private final ClientsService clientsService;
    @Autowired
    public BooksController(BooksService booksService, ClientsService clientsService) {
        this.booksService = booksService;
        this.clientsService = clientsService;
    }

    @GetMapping("/books")
    public String getBooksPage(Model model){
        List<Book> books =  booksService.getAllBooks();
        model.addAttribute("books", books);
        return "books";
    }
    @GetMapping("books/{book-id}")
    public String getBookPage(Model model, @PathVariable("book-id") Integer bookId) {
        Book book = booksService.getBook(bookId);
        model.addAttribute("book", book);
        return "book";
    }

    @PostMapping("/books")
    public String addBook(BookForm form) {

        booksService.addBook(form);
        return "redirect:/books";
    }
    @PostMapping("/books/{book-id}/delete")
    public String deleteBook(@PathVariable("book-id") Integer bookId){
        booksService.deleteBook(bookId);
        return "redirect:/books";
    }
    @PostMapping("/books/{book-id}/update")
    public String updateBook(@PathVariable("book-id") Integer bookId, BookForm bookForm){
        booksService.updateBook(bookId, bookForm);
        return "redirect:/books/{book-id}";
    }
    @GetMapping("/books/{book-id}/bookings")
    public String getBookingsByBook(Model model, @PathVariable("book-id") Integer bookId) {
        List<Booking> bookings = booksService.getBookingsByBook(bookId);
        model.addAttribute("bookings", bookings);
        List<Client> clients = clientsService.getAllClients();
        model.addAttribute("clients",clients);
        List<Book> booksList = booksService.getAllBook();
        model.addAttribute("books", booksList);
        return "booking_by_book";
    }



}
