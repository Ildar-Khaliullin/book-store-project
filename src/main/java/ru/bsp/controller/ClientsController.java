package ru.bsp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.bsp.forms.ClientForm;
import ru.bsp.models.Book;
import ru.bsp.models.Booking;
import ru.bsp.models.Client;
import ru.bsp.services.BooksService;
import ru.bsp.services.ClientsService;

import java.util.List;

@Controller
public class ClientsController {

    private final ClientsService clientsService;
    private final BooksService booksService;

    @Autowired
    public ClientsController(ClientsService clientsService, BooksService booksService, BooksService booksService1) {
        this.clientsService = clientsService;
        this.booksService = booksService1;
    }

    @GetMapping("/clients")
    public String getClientsPage(Model model){
        List<Client> clients =  clientsService.getAllClients();
        model.addAttribute("clients", clients);
        return "clients";
    }

    @PostMapping("/clients/{client-id}/delete")
    public String deleteBook(@PathVariable("client-id") Integer clientId) {
        clientsService.deleteClient(clientId);
        return "redirect:/clients";
    }

    @PostMapping("/clients")
    public String addClient(ClientForm form) {

        clientsService.addClient(form);
        return "redirect:/clients";
    }

    @GetMapping("/clients/{client-id}/bookings")
    public String getBookingsByClient(Model model, @PathVariable("client-id") Integer clientId) {
        Client client = clientsService.getClient(clientId);
        model.addAttribute("client", client);
        List<Booking> bookings = booksService.getBookingsByClient(clientId);
        model.addAttribute("bookings", bookings);
        return "booking_by_client";
    }
}
