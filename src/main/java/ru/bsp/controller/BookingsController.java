package ru.bsp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import ru.bsp.forms.BookingForm;
import ru.bsp.models.Book;
import ru.bsp.models.Booking;
import ru.bsp.models.Client;
import ru.bsp.services.BookingsService;
import ru.bsp.services.BooksService;
import ru.bsp.services.ClientsService;
import java.util.List;


@Controller
public class BookingsController {
    private final BookingsService bookingsService;
    private final BooksService booksService;
    private final ClientsService clientsService;

    public BookingsController(BookingsService bookingsService, BooksService booksService, ClientsService clientsService) {
        this.bookingsService = bookingsService;
        this.booksService = booksService;
        this.clientsService = clientsService;
    }


    @GetMapping("/books/booking_page")
    public String getBookingPage(Model model){
        List<Booking> bookings =  bookingsService.getAllBookings();
        model.addAttribute("bookings", bookings);
        List<Client> clients = clientsService.getAllClients();
        model.addAttribute("clients",clients);
        List<Book> booksList = booksService.getAllBook();
        model.addAttribute("books", booksList);
        return "booking_page";
    }

    @PostMapping("/books/booking_page/update")
    public String getAddBooking(BookingForm bookingForm){
        bookingsService.addBooking(bookingForm);
        return "redirect:/books/booking_page";
    }
}
